name 'gitlab_openvpn'
maintainer 'Brian Neel'
maintainer_email 'brian@gitlab.com'
description 'Installs/Configures OpenVPN Server'
long_description 'Installs/Configures OpenVPN Server'
version '0.1.5'
chef_version '>= 12.1' if respond_to?(:chef_version)

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
# issues_url 'https://github.com/<insert_org_here>/gitlab_vpn/issues'

# The `source_url` points to the development reposiory for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
# source_url 'https://github.com/<insert_org_here>/gitlab_vpn'

%w(apt yum line gitlab-vault).each do |cookbook|
  depends cookbook
end
