default['gitlab_openvpn']['conf_dir'] = '/etc/openvpn'
default['gitlab_openvpn']['ca_dir'] = '/etc/openvpn-ca'

default['gitlab_openvpn']['port'] = 1194
default['gitlab_openvpn']['proto'] = 'udp'
default['gitlab_openvpn']['cipher'] = 'AES-256-CBC'
# VPN address pool. Different files need different formats.
default['gitlab_openvpn']['client_address_pool'] = '10.8.0.0 255.255.255.0'
default['gitlab_openvpn']['client_address_pool_slash_format'] = '10.8.0.0/24'
default['gitlab_openvpn']['internal_ip_range'] = '10.0.0.0/8'

# DNS Servers for clients
default['gitlab_openvpn']['dns_servers'] = ["8.8.8.8", "8.8.4.4"]
# Renegotiate every 24 hours, requires re-auth
default['gitlab_openvpn']['renegotiate_timeout'] = 86400

# CA options
default['gitlab_openvpn']['key_country'] = 'US'
default['gitlab_openvpn']['key_province'] = 'CA'
default['gitlab_openvpn']['key_city'] = 'San Francisco'
default['gitlab_openvpn']['key_org'] = 'GitLab'
default['gitlab_openvpn']['key_email'] = 'operations@gitlab.com'
# This is not the hostname but rather the KEY_NAME used in the CA settings
default['gitlab_openvpn']['key_name'] = 'vpn.gitlab.net'

default['gitlab_openvpn']['ssl'] = {}
default['gitlab_openvpn']['ssl']['ca_certificate'] = 'set in chef vault'
default['gitlab_openvpn']['ssl']['ca_key'] = 'set in chef vault'
default['gitlab_openvpn']['ssl']['server_certificate'] = 'set in chef vault'
default['gitlab_openvpn']['ssl']['server_key'] = 'set in chef vault'
default['gitlab_openvpn']['ssl']['ta_key'] = 'set in chef vault'
default['gitlab_openvpn']['ssl']['dh2048'] = 'set in chef vault'
default['gitlab_openvpn']['ssl']['crl'] = 'set in chef vault'
